import React from 'react';

import Ionicons from 'react-native-vector-icons/Ionicons';

import {
  Animated,
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  StatusBar,
  View,
} from 'react-native';


import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { SafeAreaView, TabNavigator, TabBarBottom, StackNavigator } from "react-navigation";

import Home from "./containers/Home";
import Detail from "./containers/Detail";
import Settings from "./containers/Settings";

const store = configureStore({});

const SettingStack = StackNavigator(
  {
    Detail,
    Settings,
    Index: {
      screen: Settings,
    },
  },
  {
    initialRouteName: 'Index',
  }
);

const TabNav = TabNavigator(
  {
    Home: { screen: Home },
    Settings: { screen: SettingStack },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        } else if (routeName === 'Settings') {
          iconName = `ios-options${focused ? '' : '-outline'}`;
        }
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);

const app = () => (
  <Provider store={store}>    
    <TabNav />
  </Provider>
);

export default app;