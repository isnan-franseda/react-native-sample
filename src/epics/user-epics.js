import config from '../lib/config';
import {
  FETCH_DATA_ERROR,
  FETCH_DATA_REQUEST,
  FETCH_DATA_SUCCESS,
} from '../constants/action-types';

import { rx } from 'rxjs';

export const fetchUser = name => ({ type: FETCH_DATA_REQUEST });
export const fetchUserSuccessful = payload => ({ type: FETCH_DATA_SUCCESS, payload });
export const fetchError = () => ({ type: FETCH_DATA_ERROR });

const fetchUserEpic = action$ => 
  action$.ofType(FETCH_DATA_REQUEST)
    .mergeMap(action =>
      fetch(config.API_URL)
        .then((res) => res.json())
        .then((data) => fetchUserSuccessful(data))
        .catch((err) => fetchError())
    );
    
export default fetchUserEpic;