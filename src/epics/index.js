import { combineEpics, createEpicMiddleware } from 'redux-observable';
import fetchUserEpic from './user-epics'

const rootEpic = combineEpics(fetchUserEpic);

export default rootEpic;
