import { connect } from 'react-redux';
import SettingsComponent from '../../components/Settings';
import SettingsListComponent from '../../components/SettingsList';

import { getProfileSelector } from '../../reducers/user-reducer';
import { fetchUser } from '../../epics/user-epics';

const mapStateToProps = (state: Object) => getProfileSelector(state);

const mapDispatchToProps = (dispatch: Function) => ({
  fetchUser: () => dispatch(fetchUser('name')),
});

const Settings = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsListComponent);

export default Settings;
