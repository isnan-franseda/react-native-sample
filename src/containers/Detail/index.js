import React from 'react';
import { Platform, Text, View, TouchableOpacity, } from 'react-native';

class Detail extends React.Component<any, State> {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      title: params.title || 'Details Screen',
    }
  };

  render() {
    const title = this.props.navigation.state.params.title;

    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>{title}</Text>
      </View>
    );
  }
};

export default Detail;