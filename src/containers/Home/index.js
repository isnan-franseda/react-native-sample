import React from 'react';
import { Text, View } from 'react-native';

const Home = (props: Props) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Home!</Text>
    </View>
  );
};

export default Home;