import config from '../lib/config';

export const fetchUserData = () => (
  fetch(config.API_URL)
    .then((res) => res.json())
    .then((data) => data)
    .catch((err) => err)
);
