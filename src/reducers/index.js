import { combineReducers } from 'redux';
import userReducer from './user-reducer';

// Root Reducer
const rootReducer = combineReducers({
  user: userReducer,
});

export default rootReducer;
