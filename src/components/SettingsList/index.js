import React from 'react';
import {
  Animated,
  Image,
  Platform,
  ScrollView,
  TouchableOpacity,
  Text,
  StatusBar,
  View
} from 'react-native';
import { SafeAreaView, StackNavigator } from 'react-navigation';

import UserProfile from '../UserProfile';
import SettingItem from "../SettingItem";
import styles from './styles';

class SettingsListComponent extends React.Component<any, State> {
  static navigationOptions = {
    headerStyle: { position: 'absolute', backgroundColor: 'transparent', zIndex: 100, top: 0, left: 0, right: 0 },
    header: props => {
      return (
        <View />
      )
    },
  };

  componentDidMount() {
    this.props.fetchUser()
  }

  handleOpenSettingItem(title) {
    this.props.navigation.navigate('Detail', {
      title: title,
    })
  }

  openItem = item => {
    this.handleOpenSettingItem(item)
  }

  render() {
    const {
      navigation,
      isLoading,
      error,
      userInfo,
      fetchUser,
    } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <Animated.ScrollView
          style={{ flex: 1 }}
          scrollEventThrottle={1}
        >
          <SafeAreaView style={styles.backgroundUnderlay}>
            <View style={styles.contentOverlay}>
              <UserProfile name={userInfo.name} imageUrl={userInfo.imageUrl} />

              <SettingItem screen='my-wishlist' title='My Wishlist' onPressHandler={this.openItem} />
              <SettingItem screen='my-orders' title='My Orders' onPressHandler={this.openItem} />
              <SettingItem screen='my-address-book' title='My Address Book' onPressHandler={this.openItem} />
              <SettingItem screen='help-support' title='Help &amp; Support' onPressHandler={this.openItem} />
            </View>
          </SafeAreaView>
        </Animated.ScrollView>
        
        <StatusBar />
      </View>
    );
  }
}

export default SettingsListComponent;