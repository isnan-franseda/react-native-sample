import React from 'react';
import { SafeAreaView, StackNavigator } from 'react-navigation';
import {
  Image,
  Text,
  View
} from 'react-native';

const UserProfile = (props: Props) => {
  const {
    name,
    imageUrl
  } = props;

  const username = name || '...';
  const profilePicture = imageUrl || '';

  return (
    <SafeAreaView style={{
      flex: 1,
      minHeight: 200,
      flexDirection: 'row',
      alignItems: 'center'
    }}>
      <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
        <View>
          {profilePicture ? <Image style={{ width: 80, height: 80, borderRadius: 40 }} source={{ uri: profilePicture }} /> : null}
        </View>
        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20, paddingTop: 10 }}>
          {username}
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default UserProfile;