import React from 'react';
import {
  View,
  Button,
  Text,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';

type Props = {
  error: boolean,
  isLoading: boolean,
  userInfo: Object,
  fetchUser: Function
}

const getErrorMessage = () => (
  <Text style={styles.errorText}>
    An Error occured when fetching data
  </Text>
);

const getUserInfo = (userInfo) => {
  const info = userInfo ?
    userInfo.name :
    '';

  return (
    <Text style={styles.weatherInfoText}>
      {info}
    </Text>
  );
};


const SettingsComponent = (props: Props) => {
  const {
    isLoading,
    error,
    userInfo,
    fetchUser,
  } = props;
  return (
    <View style={styles.container}>
      {isLoading ? <ActivityIndicator /> : null}
      {error ? getErrorMessage() : null}
      {getUserInfo(userInfo)}
      <Button
        onPress={fetchUser}
        title='Load Data'
      />
    </View>
  );
};

export default SettingsComponent;