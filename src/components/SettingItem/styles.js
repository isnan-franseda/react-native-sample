import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  item: {
    paddingHorizontal: 16,
    paddingVertical: 20,
  },
  itemContainer: {
    backgroundColor: '#fff',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#ddd',
  },
  title: {
    fontSize: 16,
    fontWeight: 'normal',
    color: '#444',
  },
});
