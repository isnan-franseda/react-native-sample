import React from 'react';
import {
  TouchableOpacity,
  Text,
  View
} from 'react-native';
import { SafeAreaView } from 'react-navigation';
import styles from "./styles";

const SettingItem = (props: Props) => {
  const {
    screen,
    title,
    onPressHandler
  } = props;

  return (
    <TouchableOpacity onPress={() => {
      onPressHandler(title)
    }}>
      <SafeAreaView
        style={styles.itemContainer}
        forceInset={{ veritcal: 'never', bottom: 'never' }}
      >
        <View style={styles.item}>
          <Text style={styles.title}>
            {title}
          </Text>
        </View>
      </SafeAreaView>
    </TouchableOpacity>
  );
};

export default SettingItem;