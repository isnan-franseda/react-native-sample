// Redux Store Configuration
import { createStore, applyMiddleware } from 'redux';
import ReduxObservable from 'redux-observable';
import rootReducer from '../reducers';
import rootEpic from '../epics';

import { createEpicMiddleware } from 'redux-observable';

const epicMiddleware = createEpicMiddleware(rootEpic);

const configureStore = (initialState: Object) => {
  const middleware = applyMiddleware(epicMiddleware);
  return createStore(rootReducer, middleware);
};

export default configureStore;
